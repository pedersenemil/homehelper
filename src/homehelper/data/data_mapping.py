column_map = {
    "host_neighbourhood": "category",
    "host_listings_count": "float64",
    "host_has_profile_pic": "string",
    "host_identity_verified": "string",
    "neighbourhood": "category",
    "city": "category",
    "zipcode": "category",
    "latitude": "float64",
    "longitude": "float64",
    "property_type": "category",
    "room_type": "category",
    "accommodates": "int64",
    "bathrooms": "float64",
    "bedrooms": "float64",
    "beds": "float64",
    "bed_type": "category",
    "amenities": "string",
    "square_feet": "float64",
    "security_deposit": "string",
    "cleaning_fee": "string",
    "guests_included": "int64",
    "extra_people": "string",
    "minimum_nights": "int64",
    "maximum_nights": "int64",
    "calendar_updated": "string",
    "has_availability": "string",
    "availability_30": "int64",
    "availability_60": "int64",
    "availability_90": "int64",
    "availability_365": "int64",
    "instant_bookable": "string",
    "is_business_travel_ready": "string",
    "cancellation_policy": "category",
    "require_guest_profile_picture": "string",
    "require_guest_phone_verification": "string",
    "calculated_host_listings_count": "int64",
    "calculated_host_listings_count_entire_homes": "int64",
    "calculated_host_listings_count_private_rooms": "int64",
    "calculated_host_listings_count_shared_rooms": "int64",
    "host_verifications": "string",
    "price": "string",
}

column_list = list(column_map.keys())

bool_columns = [
    "host_has_profile_pic",
    "host_identity_verified",
    "has_availability",
    "instant_bookable",
    "is_business_travel_ready",
    "require_guest_profile_picture",
    "require_guest_phone_verification",
]

bool_map = {"t": True, "f": False}
