import gzip
import logging
import tempfile
from typing import Tuple

import pandas as pd
import requests
from ml_tooling.data import FileDataset

from homehelper.config import RAW_DIR
from homehelper.data.data_mapping import (
    column_list,
    column_map,
    bool_columns,
    bool_map,
)

logger = logging.getLogger("data_logger")


class PriceForecastDataset(FileDataset):
    @property
    def date(self):
        return self.file_path.name[:10]

    def _clean_target(self, target_series: pd.Series) -> pd.Series:
        """
        Price comes in a format like $365,34.00.
        We want it in a pure int format. This function cleans the target
        :param target_series: The targer series as a pd.Series
        :return: Cleaned pd.Series in int format
        """
        formatted_series = target_series.str.replace("$", "", regex=True).str.replace(
            ",", "", regex=True
        )
        # Returns Valueerror invalid literal for int() with base 10 if not done this way
        return formatted_series.astype("float").astype("int")

    def get_data(self, date=date):
        """
        Scrape data from insideairbnb.
        :param date: yyyy-mm-dd
        :return: None. Create .csv file in data/raw/
        """
        url = (
            f"http://data.insideairbnb.com"
            f"/denmark/hovedstaden/copenhagen/{date}/data/listings.csv.gz"
        )
        r = requests.get(url)
        if r.status_code == 200:
            with tempfile.NamedTemporaryFile(mode="wb") as temp:
                temp.write(r.content)
                with gzip.open(temp.name, "rt") as f:
                    (RAW_DIR.joinpath(f"{date}_copenhagen.csv").write_text(f.read()))
        else:
            logger.error(
                f"Could not retrieve data using the date: {date}. "
                f"Got response code {r.status_code}"
            )

    def load_training_data(self) -> Tuple[pd.DataFrame, pd.Series]:
        try:
            df = pd.read_csv(
                self.file_path, usecols=column_list, dtype=column_map, thousands=","
            )
            for col in bool_columns:
                df[col] = df[col].map(bool_map).astype("boolean")
            return df.drop("price", axis=1), self._clean_target(df["price"])
        except FileNotFoundError:
            logger.warning("Could not find dataset in files, trying to download file")
            try:
                self.get_data(self.date)
                df = pd.read_csv(
                    self.file_path, usecols=column_list, dtype=column_map, thousands=","
                )
                for col in bool_columns:
                    df[col] = df[col].map(bool_map).astype("boolean")
                return df.drop("price", axis=1), self._clean_target(df["price"])
            except FileNotFoundError:
                logger.error(
                    "It was not possible to create "
                    "dataset based on the date provided. "
                    f"Does the file exist on the website "
                    f"with the date {self.date}?"
                    f"Or maybe you are not pointing to the correct filepath."
                )

    def load_prediction_data(self) -> pd.DataFrame:
        pass
