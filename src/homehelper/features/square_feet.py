from ml_tooling.transformers import Select, FillNA
from sklearn.pipeline import Pipeline

square_feet_pipeline = Pipeline(
    [
        ('select_square_feet', Select("square_feet")),
        ('impute', FillNA(strategy='median')),
    ]
)
