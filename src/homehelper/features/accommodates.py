from ml_tooling.transformers import Select, FillNA
from sklearn.pipeline import Pipeline

accommodates_pipeline = Pipeline(
    [
        ('select_accommodates', Select("accommodates")),
        ('impute', FillNA(0)),
    ]
)
