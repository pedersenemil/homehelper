from ml_tooling.transformers import Select, ToCategorical
from sklearn.pipeline import Pipeline

zipcode_pipeline = Pipeline(
    [
        ('select_zipcode', Select("zipcode")),
        ('tocategorical_zipcode', ToCategorical()),
    ]
)
