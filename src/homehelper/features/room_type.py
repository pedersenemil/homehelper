from ml_tooling.transformers import Select, ToCategorical
from sklearn.pipeline import Pipeline

room_type_pipeline = Pipeline(
    [
        ('select_room_type', Select("room_type")),
        ('tocategorical_zipcode', ToCategorical()),
    ]
)
