from ml_tooling.transformers import Select, FillNA
from sklearn.pipeline import Pipeline

bedrooms_pipeline = Pipeline(
    [
        ('select_bedrooms', Select("bedrooms")),
        ('impute', FillNA(0)),
    ]
)
