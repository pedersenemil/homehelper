from ml_tooling.transformers import Select, FillNA
from sklearn.pipeline import Pipeline

bathrooms_pipeline = Pipeline(
    [
        ('select_bathrooms', Select("bathrooms")),
        ('impute', FillNA(0)),
    ]
)
