from ml_tooling.transformers import Select, ToCategorical
from sklearn.pipeline import Pipeline

bed_type_pipeline = Pipeline(
    [
        ('select_bed_type', Select("bed_type")),
        ('tocategorical_bed_type', ToCategorical()),
    ]
)
