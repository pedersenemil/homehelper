from ml_tooling.transformers import Select, ToCategorical
from sklearn.pipeline import Pipeline

property_type_pipeline = Pipeline(
    [
        ('select_property_type', Select("property_type")),
        ('tocategorical_property_type', ToCategorical()),
    ]
)
