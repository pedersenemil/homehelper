import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin


class FeatureMapper(BaseEstimator, TransformerMixin):
    """
    Maps values in a column to the provided groups.
    """

    def __init__(self, feature_map):
        """
        Parameters
        ----------
        feature_map: A dictionary map
        """
        self.feature_map = feature_map
        # self.column_name

    def fit(self, X: pd.DataFrame, y=None):
        return self

    def transform(self, X: pd.DataFrame):
        return X[X.columns[0]].map(self.feature_map)
