********
Features
********


Id
==
* Unique key for each listing
* INT
* This column is dropped


Listing_url
===========
* Link to airbnb listing
* STRING
* This column is dropped


Scrape_id
==========
* Date showing when the listing has been scraped
* INT
* This column is dropped


Date_scraped
============
* Date showing when the listing has been scraped
* STRING
* This column is dropped


Name
====
* The listing name
* STRING
* This column is dropped


Summary
=======
* Summary text of the listings characteristics
* STRING
* This column is dropped


Space
=====
* Text describing the listings characteristics.
* STRING
* This column is dropped


Description
===========
* Text describing the listings characteristics
* STRING
* This column is dropped


Experiences offered
===================
* Holds no values - empty
* This column is dropped


Neighborhood_overview
=====================
* Text describing the neighborhood
* STRING
* This column is dropped


Notes
=====
* Extra notes about the listing
* STRING
* This column is dropped


Transit
=======
* Text describing how to get to the listing address
* STRING
* This column is dropped


Access
======
* Text describing how to get access to room
* STRING
* This column is dropped


Interaction
===========
* How to get in touch, get keys, etc.
* STRING
* This column is dropped


House_rules
===========
* House rules
* STRING
* This column is dropped


Thumbnail_url
=============
* Empty
* This columns is dropped


Medium_url
==========
* Empty
* This columns is dropped.


Picture_url
===========
* URL to the listing pictures
* STRING
* This column is dropped


Host_id
=======
* ID to the host
* INT
* This column is dropped


Host_url
========
* URL link to the host user
* STRING
* This column is dropped


Host_name
=========
* Name of the host
* STRING
* This column is dropped


Host_since
==========
* First registration date of the host
* DATE
* This column is dropped. It does not make sense to provide in the prediction moment

Host_location
=============
* Location of the host
* STRING
* This column is dropped


Host_about
==========
* Description of the host
* STRING
* This column is dropped


Host_response_time
==================
* How fast does the host respond on average
* STRING
* This column is dropped. Cannot be provided in the prediction moment


Host_response_rate
==================
* How often does the host respond
* STRING
* This column is dropped. Cannot be provided in the prediction moment


Host_acceptance_rate
====================
* How often does the host accept reservations
* STRING (But is actually a boolean represented by f and t)
* This column is dropped. Cannot be provided in the prediction moment


Host_is_superhost
=================
* Is the host a superhost
* STRING (But is actually a boolean represented by f and t)
* This column is dropped. Cannot be provided in the prediction moment


Host_thumbnail_url
==================
* Link to the thumbnail image of the host
* STRING
* This column is dropped


Host_picture_url
================
* Link to the host's picture
* STRING
* This column is dropped


Host_neighborhood
=================
* Neighborhood of the host
* CATEGORICAL
* Not dropped. Should have high correlation with price


Host_listings_count
===================
* How many listings does the host have
* INT (But currently set to float?)
* Not dropped. Could have correlation with price


Host_total_listings_count
=========================
* How many listings does the host
* INT
* Duplicate of host_listings_count - same sum


Host_verifications
==================
* How is the host verified? Phone, email, reviews
* STRING (list format)
* This column is dropped. We expect that host_identity_verified provides the same correlation


Host_has_profile_pic
====================
* Does the host have a profile picture
* STRING (A boolean represented by f and t)
* Not dropped. Could have correlation with price


Host_identity_verified
======================
* Has the host verified his identity
* STRING (A boolean represented by f and t)
* Not dropped. Could have correlation with price


Street
======
* Duplicate values - city names
* STRING
* This column is dropped


Neighbourhood
=============
* Which neigborhood is the listing located in
* CATEGORICAL
* Not dropped. Should have high correlation with price. Might provide the same correlation as zipcode


Neighbourhood_cleansed
======================
* Neighborhood values but without æ,ø,å
* CATEGORICAL
* This column is dropped


Neighbourhood_group_cleansed
============================
* EMPTY
* This column is dropped


City
====
* Cityname
* CATEGORICAL
* Not dropped


State
=====
* Contains regions - not states
* CATEGORICAL
* This column is dropped. Overlaps with city and neighborhood


Zipcode
=======
* Zipcode of the listing
* CATEGORICAL
* Not dropped. Should have high correlation with price


Country
=======
* Which country is the listing located in
* CATEGORICAL
* This column is dropped. We only work with danish data


Latitude
========
* Coordinate
* STRING
* Not dropped. Might have correlation somehow


Longtitude
==========
* Coordinate
* STRING
* Not dropped. Might have correlation somehow


Is_location_exact
=================
* Is the location exact
* STRING (A boolean represented by f and t)
* This column is dropped

Property_type
=============
* Apartment, house, etc
* CATEGORICAL
* Not dropped. Should have high correlation with price

Room_type
=========
* Entire flat, private room, etc
* CATEGORICAL
* Not dropped. Should have high correlation with price

Accommodates
============
* How many can stay in the listing
* INT
* Not dropped. Should have correlation with price

Bathrooms
=========
* Number of bathrooms
* FLOAT
* Not dropped. Should have correlation with price

Bedrooms
========
* Number of bedrooms
* INT
* Not dropped. Should have correlation with price

Beds
====
* Number of beds
* INT
* Not dropped. Should have correlation with price

Bed_type
========
* Type of bed
* CATEGORICAL
* Not dropped. Should have correlation with price

Amenities
=========
* Bonus features. Which objects are present in the listing. TV, cutlery, etc
* STRING (List format)
* Not dropped. Could have correlation with price
* The column contains a list of all the features that characterize the listing. Contains up to 71 different values.


Square_feet
===========
* Size of the listing
* INT
* Not dropped. Should have correlation with price

Price
=====
* Price for one day
* STRING (Formatted as $100.00)
* Not dropped. This is our target value. Removed from training data

Weekly_price
============
* Price for a week
* STRING
* This column is dropped

Monthly_price
=============
* Price for a month
* STRING
* This column is dropped

Security_deposit
================
* Price for the deposit
* STRING
* Not dropped. Could have correlation with price

Cleaning_fee
============
* Price for cleaning after stay
* STRING
* Not dropped. Could have correlation with price

Guests_included
===============
* Number of guests can stay there
* INT
* Not dropped. Could have correlation with price

Extra_people
============
* How much for extra guests
* STRING
* Not dropped. Could have correlation with price

Minimum_nights
==============
* Minimum number of nights that can be booked
* INT
* Not dropped. Could have correlation with price


Maximum_nights
==============
* Maximum number of nights that can be booked
* INT
* Not dropped. Could have correlation with price


Minimum_minimum_nights
======================
* INT
* This column is dropped


Maximum_minimum_nights
======================
* INT
* This column is dropped


Maximum_maximum_nights
======================
* INT
* This column is dropped


Minimum_nights_avg_ntm
======================
* FLOAT
* This column is dropped


Calendar_updated
================
* When has the host last updated his calendar
* STRING
* Not dropped. Could have correlation with price


Has_availability
================
* Are there any available nights
* STRING (A boolean represented by f and t)
* Not dropped. Could have correlation with price


Availability_30
===============
* Number of available days within 30 days
* INT
* Not dropped. Could have correlation with price


Availability_60
===============
* Number of available days within 60 days
* INT
* Not dropped. Could have correlation with price


Availability_90
===============
* Number of available days within 90 days
* INT
* Not dropped. Could have correlation with price


Availability_365
================
* Number of available days within 365 days
* INT
* Not dropped. Could have correlation with price


Calendar_last_scraped
=====================
* When has the calendar last been scraped
* STRING (But is actually a date)
* This column is dropped


Number_of_reviews
=================
* How many reviews has the host received
* INT
* This column is dropped. Cannot be provided in the prediction moment


Number_of_reviews_ltm
=====================
* Number of reviews within the last twelve months (ltm)
* INT
* This column is dropped. Cannot be provided in the prediction moment


First_review
============
* When did the host receive his first review
* STRING (But actually a date)
* This column is dropped. Cannot be provided in the prediction moment


Last_review
===========
* When did the host receive his last review
* STRING (But actually a date)
* This column is dropped. Cannot be provided in the prediction moment


Review_scores_rating
====================
* A rating score between 0 and 100
* INT
* This column is dropped. Cannot be provided in the prediction moment


Review_scores_accuracy
======================
* Review score accuracy
* INT
* This column is dropped. Cannot be provided in the prediction moment


Review_scores_cleanliness
=========================
* Score of the listings cleanlines
* INT
* This column is dropped. Cannot be provided in the prediction moment


Review_scores_checkin
=====================
* How is the checkin scored
* INT
* This column is dropped. Cannot be provided in the prediction moment


Review_scores_location
======================
* How is the location scored
* INT
* This column is dropped. Cannot be provided in the prediction moment


Review_scores_value
===================
* Review score value
* INT
* This column is dropped. Cannot be provided in the prediction moment


Review_scores_license
=====================
* Review score license
* INT
* This column is dropped


License
=======
* Empty
* This column is dropped


Jurisdiction_names
==================
* Empty
* This column is dropped


Instant_bookable
================
* Can the listing be instantly booked
* STRING (A boolean represented by f and t)
* Not dropped. Could have correlation with price


Is_business_travel_ready
========================
* Is the listing ready for business travelers
* STRING (A boolean represented by f and t)
* Not dropped. Could have correlation with price


Cancellation_policy
===================
* How is the hosts cancellation policy
* CATEGORICAL
* Not dropped. Could have correlation with price


Require_guest_profile_picture
=============================
* Does the host require the guest to have a profile picture
* STRING (A boolean represented by f and t)
* Not dropped. Could have correlation with price


Require_guest_phone_verification
================================
* Does the host require the guest to have his phone number verified
* STRING (A boolean represented by f and t)
* Not dropped. Could have correlation with price


Calculated_host_listings_count
==============================
* A calculation of how many listings the host has
* INT
* Not dropped. Could have correlation with price


Calculated_host_listings_count_private_rooms
============================================
* A calculation of how many private rooms the host has
* INT
* Not dropped. Could have correlation with price


Calculated_host_listings_count_shared_rooms
===========================================
* A calculation of how many shared rooms the host has
* INT
* Not dropped. Could have correlation with price


Reviews_per_month
=================
* An average of how many reviews the host receives per month
* FLOAT
* This column is dropped. Cannot be provided in the prediction moment
