import pytest

from homehelper.data.dataset import PriceForecastDataset


@pytest.fixture()
def testdata_as_df():
    data = PriceForecastDataset("tests/testdata_2020-06-26_copenhagen.csv")
    x, y = data.load_training_data()
    return x, y
