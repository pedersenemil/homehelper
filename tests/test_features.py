import pandas as pd

from homehelper.features.accommodates import accommodates_pipeline
from homehelper.features.bathrooms import bathrooms_pipeline
from homehelper.features.bed_type import bed_type_pipeline
from homehelper.features.bedrooms import bedrooms_pipeline
from homehelper.features.property_type import property_type_pipeline
from homehelper.features.room_type import room_type_pipeline
from homehelper.features.zipcode import zipcode_pipeline


def test_accomodates_pipeline(testdata_as_df):
    x, y = testdata_as_df

    transformed_x = accommodates_pipeline.fit_transform(x)

    assert len(transformed_x) == 1000
    assert isinstance(transformed_x, pd.DataFrame)
    assert transformed_x['accommodates'].dtype == 'int64'


def test_bathrooms_pipeline(testdata_as_df):
    x, y = testdata_as_df

    transformed_x = bathrooms_pipeline.fit_transform(x)

    assert len(transformed_x) == 1000
    assert isinstance(transformed_x, pd.DataFrame)
    assert transformed_x['bathrooms'].dtype == 'float64'


def test_bed_type_pipeline(testdata_as_df):
    x, y = testdata_as_df

    transformed_x = bed_type_pipeline.fit_transform(x)

    assert len(transformed_x) == 1000
    assert isinstance(transformed_x, pd.DataFrame)
    assert transformed_x.max().max() == 1


def test_bedrooms_pipeline(testdata_as_df):
    x, y = testdata_as_df

    transformed_x = bedrooms_pipeline.fit_transform(x)

    assert len(transformed_x) == 1000
    assert isinstance(transformed_x, pd.DataFrame)
    assert transformed_x['bedrooms'].dtype == 'float64'


def test_property_type_pipeline(testdata_as_df):
    x, y = testdata_as_df

    transformed_x = property_type_pipeline.fit_transform(x)

    assert len(transformed_x) == 1000
    assert isinstance(transformed_x, pd.DataFrame)
    assert transformed_x.max().max() == 1


def test_room_type_pipeline(testdata_as_df):
    x, y = testdata_as_df

    transformed_x = room_type_pipeline.fit_transform(x)

    assert len(transformed_x) == 1000
    assert isinstance(transformed_x, pd.DataFrame)
    assert transformed_x.max().max() == 1


def test_zipcode_pipeline(testdata_as_df):
    x, y = testdata_as_df

    transformed_x = zipcode_pipeline.fit_transform(x)

    assert len(transformed_x) == 1000
    assert isinstance(transformed_x, pd.DataFrame)
    assert transformed_x.max().max() == 1
